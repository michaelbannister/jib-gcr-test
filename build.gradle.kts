plugins {
    kotlin("jvm").version("1.3.70")
    id("com.google.cloud.tools.jib").version("2.1.0")
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.1")

    testImplementation("ch.tutteli.atrium:atrium-fluent-en_GB:0.10.0")
    testImplementation("ch.tutteli.atrium:atrium-api-fluent-en_GB-kotlin_1_3:0.10.0")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

jib {
    container {
        mainClass = "org.bannisters.AppKt"
    }
    from {
        image = "eu.gcr.io/michaelbannister-jib-gcr-test/distroless-jvm:11"
    }
    to {
        image = "eu.gcr.io/michaelbannister-jib-gcr-test/jib-gcr-test:${System.getenv("CI_PIPELINE_IID") ?: "local"}"
    }
}